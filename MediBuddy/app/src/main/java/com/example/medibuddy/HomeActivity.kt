package com.example.medibuddy

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_main.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val intent = intent
        val receivedEmail = intent.getStringExtra("emailAddress")
        nameResponse.text = receivedEmail

        button_logout.setOnClickListener{
            Firebase.auth.signOut()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        button_go_current.setOnClickListener{
            val intent = Intent(this, CurrentActivity::class.java)
            startActivity(intent)
        }

        button_go_set.setOnClickListener{
            val intent = Intent(this, ReminderActivity::class.java)
            startActivity(intent)
        }

        button_go_progress.setOnClickListener{
            val intent = Intent(this, ProgressActivity::class.java)
            startActivity(intent)
        }

    }
}
