package com.example.medibuddy

import android.app.AlertDialog
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.progress_list_layout.view.*

class ProgressAdapter (
    private val medicineList: ArrayList<Medicine>) : RecyclerView.Adapter<ProgressAdapter.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bindItems (medi: Medicine){
            val medicineName = itemView.findViewById(R.id.card_view_medicine_field_progress) as TextView
            //val startDate = itemView.findViewById(R.id.card_view_s_t_field) as TextView
            //val endDate = itemView.findViewById(R.id.card_view_e_t_field) as TextView
            val count = itemView.findViewById(R.id.card_view_count_progress) as TextView
            val percentage = itemView.findViewById(R.id.card_view_percentage_field) as TextView
            val alarmTime = itemView.findViewById(R.id.card_view_clock_progress) as TextView

            medicineName.text = medi.medicine_name
            //count.text = medi.time_range
            alarmTime.text = medi.alarm_time
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProgressAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.progress_list_layout, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder_2: ViewHolder, position: Int) {
        holder_2.bindItems(medi = medicineList[position])
        holder_2.itemView.card_view_delete_btn_progress.setOnClickListener {
            val builder = AlertDialog.Builder(holder_2.itemView.context)
            builder.setTitle(" ")
            builder.setIcon(R.drawable.ic_round_warning_24)
            builder.setMessage("If you proceed, both reminder and your progress will be deleted permanently. Are you sure that you want to delete this entry?")
            builder.setPositiveButton("Yes") { dialogInterface: DialogInterface, i: Int ->
                val dbUserData = DBUserData(holder_2.itemView.context)
                val mid = medicineList[position].medicineId.toString()
                dbUserData.deleteAlarm(cid = mid)
                ProgressActivity.medArray.removeAt(position)
                notifyDataSetChanged()
            }
            builder.setNegativeButton("No") { dialogInterface: DialogInterface, i: Int -> }
            builder.show()
        }
    }

    override fun getItemCount(): Int {
        return medicineList.size
    }

}