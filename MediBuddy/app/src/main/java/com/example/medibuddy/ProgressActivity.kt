package com.example.medibuddy

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_progress.*
import kotlinx.android.synthetic.main.activity_home.*

class ProgressActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_progress)

        val recyclerView = findViewById<RecyclerView>(R.id.recycler_progress)

        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

//        val bundle: Bundle? = intent.extras
//        val string: String? = intent.getStringExtra("MEDICINE_LIST")
//        val medicineArray: ArrayList<Medicine>? = intent.getParcelableArrayListExtra("MEDICINE_LIST")

        //Log.d("KKKK",medicineArray.toString())

        val dbUserData = DBUserData(this)

        medArray = dbUserData.fetchAllData()
        Log.d("KKKK", medArray.toString())

        if (!medArray.isNullOrEmpty()) {
            val adapter = ProgressAdapter(medArray)
            recyclerView.adapter = adapter
            //adapter.notifyDataSetChanged()
        }
    }

    companion object {
        var medArray = ArrayList<Medicine>()
    }

}

