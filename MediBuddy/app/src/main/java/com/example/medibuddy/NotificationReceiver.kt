package com.example.medibuddy

import android.app.*
import android.content.BroadcastReceiver
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.media.Ringtone
import android.media.RingtoneManager
import android.media.audiofx.Virtualizer
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import java.security.AccessController.getContext

class NotificationReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            // Create the NotificationChannel
//            val name = "Alarm"
//            val descriptionText = "Alarm details"
//            val importance = NotificationManager.IMPORTANCE_DEFAULT
//            val mChannel = NotificationChannel("AlarmId", name, importance)
//            mChannel.description = descriptionText
//            val notificationManager = context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//            notificationManager.createNotificationChannel(mChannel)
//        }

        val resultIntent = Intent(context, SplashActivity::class.java)
        // Create the TaskStackBuilder
        val resultPendingIntent: PendingIntent? = TaskStackBuilder.create(context).run {
            // Add the intent, which inflates the back stack
            addNextIntentWithParentStack(resultIntent)
            // Get the PendingIntent containing the entire back stack
            getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel("AlarmId",
                "Alarm",
                NotificationManager.IMPORTANCE_DEFAULT)

            val attributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()

            // Configure the notification channel.
            mChannel.description = "Alarm details"
            mChannel.enableLights(true);
            mChannel.enableVibration(true);
            mChannel.setSound(Uri.parse(
                ("android.resource://"
                        + context.packageName) + "/"
                        + R.raw.relaxing_tone
            ), attributes) // This is IMPORTANT

            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)

        }
            val builder = NotificationCompat.Builder(context, "AlarmId")
                .setContentTitle("Time to take your medication").setStyle(
                    NotificationCompat.BigTextStyle()
                        .bigText("Please visit the app to see which medication you should took now.")
                )
                .setContentText("Please visit the app to see which medication you should took now.")
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
                .setSound(
                    Uri.parse(
                        ("android.resource://"
                                + context.packageName) + "/"
                                + R.raw.relaxing_tone
                    )
                )
                .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
                .setDefaults(Notification.DEFAULT_LIGHTS and Notification.DEFAULT_VIBRATE and Notification.DEFAULT_SOUND)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
            val notification: Notification = builder.build()
            val id = System.currentTimeMillis() / 1000
            val notificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(id.toInt(), notification)



//        // Create the notification to be shown
//        val mBuilder = NotificationCompat.Builder(context!!, "AlarmId")
//            .setSmallIcon(R.mipmap.ic_launcher)
//            .setContentTitle("Time to take your medication")
//            .setStyle(NotificationCompat.BigTextStyle().bigText("Please visit the app to see which medication you should took now."))
//            .setContentText("Please visit the app to see which medication you should took now.")
//            .setAutoCancel(true)
//            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//            .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
//
//
//        // Get the Notification manager service
//        val am = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//        // Generate an Id for each notification
//        val id = System.currentTimeMillis() / 1000
//
//        // Show a notification
//        am.notify(id.toInt(), mBuilder.build())
    }
}