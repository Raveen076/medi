package com.example.medibuddy

import android.app.AlertDialog
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_layout.view.*

class MedicineAdapter (
    private val medicineList: ArrayList<Medicine>) : RecyclerView.Adapter<MedicineAdapter.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bindItems (medi: Medicine){
            val medicineName = itemView.findViewById(R.id.card_view_medicine_field) as TextView
            val medicineQuantity = itemView.findViewById(R.id.card_view_quantity_field) as TextView
            val consumeTime = itemView.findViewById(R.id.card_view_image) as ImageView
            val startDate = itemView.findViewById(R.id.card_view_s_t_field) as TextView
            val endDate = itemView.findViewById(R.id.card_view_e_t_field) as TextView
            val alarmTime = itemView.findViewById(R.id.card_view_clock) as TextView

            medicineName.text = medi.medicine_name
            medicineQuantity.text = medi.medicine_quantity
            if(medi.before_meal == "before") {
                consumeTime.setImageResource(R.drawable.ic_outline_not_interested_24)
            } else {
                consumeTime.setImageResource(R.drawable.ic_outline_fastfood_24)
            }
            startDate.text = medi.start_date
            endDate.text = medi.end_date
            alarmTime.text = medi.alarm_time
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicineAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_layout, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(medi = medicineList[position])
        holder.itemView.card_view_delete_btn.setOnClickListener {
            val builder = AlertDialog.Builder(holder.itemView.context)
            builder.setTitle(" ")
            builder.setIcon(R.drawable.ic_round_warning_24)
            builder.setMessage("If you proceed, both reminder and your progress will be deleted permanently. Are you sure that you want to delete this entry?")
            builder.setPositiveButton("Yes") { dialogInterface: DialogInterface, i: Int ->
                val dbUserData = DBUserData(holder.itemView.context)
                val mid = medicineList[position].medicineId.toString()
                dbUserData.deleteAlarm(cid = mid)
                CurrentActivity.medArray.removeAt(position)
                notifyDataSetChanged()
            }
            builder.setNegativeButton("No") { dialogInterface: DialogInterface, i: Int -> }
            builder.show()
        }
        holder.itemView.card_view_confirm_btn.setOnClickListener {
            val builder = AlertDialog.Builder(holder.itemView.context)
            builder.setTitle("Did you took your medication on time?")
            builder.setIcon(R.drawable.ic_round_warning_24)
            builder.setMessage("Press 'Yes' only if took your daily dose. Else please press 'No' and take it now!")
            builder.setPositiveButton("Yes") { dialogInterface: DialogInterface, i: Int ->
                holder.itemView.card_view_confirm_btn.visibility = View.GONE
            }
            builder.setNegativeButton("No") { dialogInterface: DialogInterface, i: Int -> }
            builder.show()
        }
    }

    override fun getItemCount(): Int {
        return medicineList.size
    }

}