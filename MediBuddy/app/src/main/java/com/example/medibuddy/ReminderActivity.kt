package com.example.medibuddy

import android.app.AlarmManager
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_reminder.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.xml.datatype.DatatypeConstants.MONTHS
import kotlin.collections.ArrayList
import kotlin.math.log
import android.content.ComponentName
import android.content.Context
import java.lang.String.format

class ReminderActivity : AppCompatActivity() {

    var startDate : String = ""
    var endDate: String = ""

    val medicines = ArrayList<Medicine>()

    private lateinit var medicineNameEditText: EditText
    private lateinit var medicineQuantityEditText: EditText
    private lateinit var startTimeEditBtn: Button
    private lateinit var endTimeEditBtn: Button
    private lateinit var alarmTimeBtn: Button
    private lateinit var beforeMealImage: ImageView
    private lateinit var afterMealImage: ImageView
    private lateinit var timeSpan: TextView
    private var beforeMeal: String = "before"
    private var isMealTimeSelected = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reminder)

        medicineNameEditText = findViewById(R.id.medicine_field)
        medicineQuantityEditText = findViewById(R.id.quantity_field)
        startTimeEditBtn = findViewById(R.id.button_set_first_date)
        endTimeEditBtn = findViewById(R.id.button_set_last_date)
        alarmTimeBtn = findViewById(R.id.button_set_time)
        beforeMealImage = findViewById(R.id.img_no_food)
        afterMealImage = findViewById(R.id.img_food)
        timeSpan = findViewById(R.id.time_difference)

        drawableSelected()

        var timeInMilliSeconds: Long = 0
        val receiver = ComponentName(applicationContext, BootCompleteReceiver::class.java)

        applicationContext.packageManager?.setComponentEnabledSetting(
            receiver,
            PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
            PackageManager.DONT_KILL_APP
        )

        button_set_time.setOnClickListener {
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener{timePicker , hour , minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                cal.set(Calendar.SECOND, 0)
                cal.set(Calendar.MILLISECOND, 0)
                button_set_time.text = SimpleDateFormat("HH : mm").format(cal.time)
                val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault())
                val formattedDate = sdf.format(cal.time)
                val date = sdf.parse(formattedDate)
                timeInMilliSeconds = date.time
            }
            TimePickerDialog(this, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show()
        }


        button_set_first_date.setOnClickListener {
            val c1 = Calendar.getInstance()
            val year1 = c1.get(Calendar.YEAR)
            val month1 = c1.get(Calendar.MONTH)
            val day1 = c1.get(Calendar.DAY_OF_MONTH)
            val dpd1 = DatePickerDialog(this, DatePickerDialog.OnDateSetListener{view, mYear, mMonth, mDay->
                button_set_first_date.setText("" + mYear + " / " + (mMonth+1) +" / "+ mDay)
            }, year1, month1, day1)
            startDate = button_set_first_date.text.toString()
            dpd1.show()
        }

        button_set_last_date.setOnClickListener {
            val c2 = Calendar.getInstance()
            val year2 = c2.get(Calendar.YEAR)
            val month2 = c2.get(Calendar.MONTH)
            val day2 = c2.get(Calendar.DAY_OF_MONTH)
            val dpd2 = DatePickerDialog(this, DatePickerDialog.OnDateSetListener{view, mYear, mMonth, mDay ->
                button_set_last_date.setText("" + mYear + " / " + (mMonth+1) +" / "+ mDay)
            }, year2, month2, day2)
            endDate = button_set_last_date.text.toString()
            dpd2.show()
        }

        button_set_alarm.setOnClickListener {
            storeMedicineStatus()
            Log.d("MMM", medicines.toString())
            if (timeInMilliSeconds.toInt() != 0) {
                Toast.makeText(this, "Alarm has been set!", Toast.LENGTH_LONG).show()

                val sharedPref = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
                    ?: return@setOnClickListener
                with(sharedPref.edit()) {
                    putLong("timeInMilli", timeInMilliSeconds)
                    apply()
                }
                Utils.setAlarm(this, timeInMilliSeconds)
            } else {
                Toast.makeText(this, "Please enter the time first!", Toast.LENGTH_LONG).show()
            }
        }
    }

    private  fun storeMedicineStatus() {
        if (medicineNameEditText.text.isNullOrEmpty() ||
            medicineQuantityEditText.text.isNullOrEmpty() ||
            startTimeEditBtn.text == "Starting Date" ||
            endTimeEditBtn.text == "Ending Date"
            || alarmTimeBtn.text == "Time" ||
                !isMealTimeSelected) {
                Toast.makeText(this, "Please fill all fields to set an alarm.", Toast.LENGTH_LONG).show()
        } else {

//            medicines.add(Medicine(medicine_name = medicineNameEditText.text.toString(),
//                medicine_quantity = medicineQuantityEditText.text.toString(),
//                start_date = startTimeEditBtn.text.toString(),
//                end_date = endTimeEditBtn.text.toString(),
//                alarm_time = alarmTimeBtn.text.toString(),
//                before_meal = beforeMeal
//            ))

            val dbhelper = DBUserData(this)
            dbhelper.create_db_table
            val insert = dbhelper.insertUserDetails(Medicine(
                medicineId = null,
                medicine_name = medicineNameEditText.text.toString(),
                medicine_quantity = medicineQuantityEditText.text.toString(),
                start_date = startTimeEditBtn.text.toString(),
                end_date = endTimeEditBtn.text.toString(),
                alarm_time = alarmTimeBtn.text.toString(),
                before_meal = beforeMeal
            ))

            if (insert) {
                val intent = Intent(this, CurrentActivity::class.java)
                //intent.putParcelableArrayListExtra("MEDICINE_LIST", medicines)
                startActivity(intent)
                finish()
            }
        }
    }

    private var ds = 0

    private fun drawableSelected() {
        img_food.setOnClickListener {
            img_food.isSelected = !img_food.isSelected
            ds = R.drawable.ic_outline_fastfood

            img_no_food.isSelected = false
            beforeMeal = "after"
            isMealTimeSelected = true
        }

        img_no_food.setOnClickListener {
            img_no_food.isSelected = !img_no_food.isSelected
            ds = R.drawable.ic_outline_not_interested

            img_food.isSelected = false
            beforeMeal = "before"
            isMealTimeSelected = true
        }
    }
}
